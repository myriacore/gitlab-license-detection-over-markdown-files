# Testing Markdown License Files

This project is used in gitlab-org/gitlab#219579 for showcasing GitLab's inability 
to detect the license information of markdown license files, as used
by [evil-surround mode](https://github.com/emacs-evil/evil-surround/blob/1c34944d8c98da4a2385d24ee89eef9cdf569a12/license.md)
and many other prominent projects.

[LICENSE.md](LICENSE.md) is a common version of the GPLv3. 